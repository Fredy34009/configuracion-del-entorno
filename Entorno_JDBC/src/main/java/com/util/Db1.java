/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.util;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 *
 * @author FREDY
 */
public class Db1 {

    private Connection conn = null;
    private String db = "db1";
    private String pass = "root";
    private String user = "root";
    private String url = "jdbc:mysql://localhost:3306/"+db+"?useSSL=false";
    private String driver = "com.mysql.jdbc.Driver";

    public Db1() {
    }

    public Connection conectar() {
        try {
            Class.forName(driver);
            conn = DriverManager.getConnection(url, user, pass);
            if (conn != null) {
                System.out.println("Exito");
            }
            return conn;
        } catch (Exception e) {
             return null;
        }
       
    }
    public static void main(String[] args) {
        Db1 d=new Db1();
        d.conectar();
    }
}
