/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.control;

import com.dao.Dao;
import com.dao.EmpleadosDao;
import com.modelo.Empleados;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author FREDY
 */
@ManagedBean
@SessionScoped
public class EmpleadosMB {

    private Dao dao;
    private List<Empleados> empleados;
    private Empleados empleado;
    private Empleados jefe;

    public EmpleadosMB() {
        dao = new EmpleadosDao();
        empleado = new Empleados();
        jefe=new Empleados();
    }
    public void limpiar()
    {
        empleado = new Empleados();
        jefe=new Empleados();
    }
    public void deleteRespaldo()
    {
        dao.deleteRespaldo();
    }

    public void consultar() {
        empleados = dao.consultar();
    }
    public void editEmpleado()
    {
        try {
            empleado.setJefe(jefe);
            dao.editEmpleado(empleado);
            limpiar();
        } catch (Exception e) {
        }
    }
    public void insertEmpleado()
    {
        try {
            empleado.setJefe(jefe);
            dao.insertEmpleado(empleado);
            limpiar();
        } catch (Exception e) {
        }
    }
    public void borrar(Empleados em)
    {
        try {
            dao.borar(em);
        } catch (Exception e) {
        }
    }
    public void seleccion(Empleados e)
    {
        this.empleado=e;
        this.jefe.setId(e.getJefe().getId());
    }

    public void migrar() {
        Empleados jefe = new Empleados();
        try {
            for (Empleados emp : empleados) {
                empleado.setId(emp.getId());
                empleado.setPrimerNombre(emp.getPrimerNombre());
                empleado.setSegundoNombre(emp.getSegundoNombre());
                empleado.setPrimerApellido(emp.getPrimerApellido());
                empleado.setSegundoApellido(emp.getSegundoApellido());
                empleado.setCorreo(emp.getCorreo());
                empleado.setTelFijo(emp.getTelFijo());
                empleado.setTelMovil(emp.getTelMovil());
                empleado.setJefe(emp.getJefe());
                dao.migrarDatos(empleado);
            }
            
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public List<Empleados> getEmpleados() {
        empleados = dao.consultar();
        return empleados;
    }

    public void setEmpleados(List<Empleados> empleados) {
        this.empleados = empleados;
    }

    public Empleados getEmpleado() {
        return empleado;
    }

    public void setEmpleado(Empleados empleado) {
        this.empleado = empleado;
    }

    public Empleados getJefe() {
        return jefe;
    }

    public void setJefe(Empleados jefe) {
        this.jefe = jefe;
    }
    
}
