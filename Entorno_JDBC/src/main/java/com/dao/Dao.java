/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dao;

import com.modelo.Empleados;
import java.util.List;

/**
 *
 * @author FREDY
 */
public interface Dao {

    public List<Empleados> consultar();

    public boolean borar(Empleados em);

    public boolean migrarDatos(Empleados e);

    public boolean insertEmpleado(Empleados e);

    public boolean deleteRespaldo();

    public boolean editEmpleado(Empleados e);
}
