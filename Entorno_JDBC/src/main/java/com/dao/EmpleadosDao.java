/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dao;

import com.modelo.Empleados;
import com.util.Db1;
import com.util.Db2;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author FREDY
 */
public class EmpleadosDao implements Dao {

    Db1 da = new Db1();
    Db2 d2 = new Db2();
    PreparedStatement ps;
    ResultSet rs;
    Empleados empleados;
    Empleados jefe;

    //Consulta todos los enmpleados de db1
    @Override
    public List<Empleados> consultar() {
        try {
            ps = da.conectar().prepareStatement("select * from empleados");
            rs = ps.executeQuery();
            List<Empleados> lista = new ArrayList<>();
            while (rs.next()) {
                jefe = new Empleados();
                jefe.setId(rs.getInt("jefe"));
                empleados = new Empleados();
                empleados.setId(rs.getInt("id"));
                empleados.setPrimerNombre(rs.getString("primer_nombre"));
                empleados.setSegundoNombre(rs.getString("segundo_nombre"));
                empleados.setPrimerApellido(rs.getString("primer_apellido"));
                empleados.setSegundoApellido(rs.getString("segundo_apellido"));
                empleados.setCorreo(rs.getString("correo"));
                empleados.setTelFijo(rs.getString("tel_fijo"));
                empleados.setTelMovil(rs.getString("tel_movil"));
                empleados.setJefe(jefe);
                lista.add(empleados);
            }
            return lista;
        } catch (SQLException ex) {
            Logger.getLogger(EmpleadosDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    //Borra un empleado de db1
    @Override
    public boolean borar(Empleados em) {

        try {
            ps = da.conectar().prepareStatement("delete from empleados where id=?");
            ps.setInt(1, em.getId());
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    //Consulta por id un empleado de db1
    public List<Empleados> consultarXId(Empleados e) {
        try {
            ps = da.conectar().prepareStatement("select * from empleados where id=" + e.getId());
            rs = ps.executeQuery();
            List<Empleados> lista = new ArrayList<>();
            while (rs.next()) {
                jefe = new Empleados();
                jefe.setId(rs.getInt("jefe"));
                empleados = new Empleados();
                empleados.setId(rs.getInt("id"));
                empleados.setPrimerNombre(rs.getString("primer_nombre"));
                empleados.setSegundoNombre(rs.getString("segundo_nombre"));
                empleados.setPrimerApellido(rs.getString("primer_apellido"));
                empleados.setSegundoApellido(rs.getString("segundo_apellido"));
                empleados.setCorreo(rs.getString("correo"));
                empleados.setTelFijo(rs.getString("tel_fijo"));
                empleados.setTelMovil(rs.getString("tel_movil"));
                empleados.setJefe(jefe);
                lista.add(empleados);
            }
            return lista;
        } catch (SQLException ex) {
            Logger.getLogger(EmpleadosDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    //Metodo que inserta los registros de db1 a db2
    @Override
    public boolean migrarDatos(Empleados e) {
        try {
            jefe = e.getJefe();
            if (jefe.getId() == 0) {
                ps = d2.conectar().prepareStatement("insert into empleados(id,primer_nombre,segundo_nombre,"
                        + " primer_apellido,segundo_apellido,correo,tel_fijo,tel_movil) values(?,?,?,?,?,?,?,?)");
                ps.setInt(1, e.getId());
                ps.setString(2, e.getPrimerNombre());
                ps.setString(3, e.getSegundoNombre());
                ps.setString(4, e.getPrimerApellido());
                ps.setString(5, e.getSegundoApellido());
                ps.setString(6, e.getCorreo());
                ps.setString(7, e.getTelFijo());
                ps.setString(8, e.getTelMovil());
            } else {
                jefe = e.getJefe();
                ps = d2.conectar().prepareStatement("insert into empleados values(?,?,?,?,?,?,?,?,?)");
                ps.setInt(1, e.getId());
                ps.setString(2, e.getPrimerNombre());
                ps.setString(3, e.getSegundoNombre());
                ps.setString(4, e.getPrimerApellido());
                ps.setString(5, e.getSegundoApellido());
                ps.setString(6, e.getCorreo());
                ps.setString(7, e.getTelFijo());
                ps.setString(8, e.getTelMovil());
                ps.setInt(9, jefe.getId());
            }
            ps.executeUpdate();
            return true;
        } catch (Exception ex) {
            System.out.println("Error dao " + ex.getMessage());
            return false;
        }
    }

    //Inserta un empleado en db1
    @Override
    public boolean insertEmpleado(Empleados e) {
        try {
            jefe = e.getJefe();
            if (jefe.getId() == 0) {
                ps = da.conectar().prepareStatement("insert into empleados(primer_nombre,segundo_nombre,"
                        + " primer_apellido,segundo_apellido,correo,tel_fijo,tel_movil) values(?,?,?,?,?,?,?)");
                ps.setString(1, e.getPrimerNombre());
                ps.setString(2, e.getSegundoNombre());
                ps.setString(3, e.getPrimerApellido());
                ps.setString(4, e.getSegundoApellido());
                ps.setString(5, e.getCorreo());
                ps.setString(6, e.getTelFijo());
                ps.setString(7, e.getTelMovil());
            } else {

                ps = da.conectar().prepareStatement("insert into empleados(primer_nombre,segundo_nombre,"
                        + " primer_apellido,segundo_apellido,correo,tel_fijo,tel_movil,jefe) values(?,?,?,?,?,?,?,?)");
                ps.setString(1, e.getPrimerNombre());
                ps.setString(2, e.getSegundoNombre());
                ps.setString(3, e.getPrimerApellido());
                ps.setString(4, e.getSegundoApellido());
                ps.setString(5, e.getCorreo());
                ps.setString(6, e.getTelFijo());
                ps.setString(7, e.getTelMovil());
                ps.setInt(8, jefe.getId());
            }
            ps.executeUpdate();
            return true;
        } catch (Exception ex) {
            System.out.println("Error dao " + ex.getMessage());
            return false;
        }
    }

    //Metodo que borra la data de DB2
    @Override
    public boolean deleteRespaldo() {
        try {
            ps = d2.conectar().prepareStatement("truncate table empleados");
            ps.executeUpdate();
            System.out.println("Si la borre");
            return true;
        } catch (Exception e) {
            return false;
        }
    }
    //Actualiza un empleado en db1
    @Override
    public boolean editEmpleado(Empleados e) {
        try {
            jefe = e.getJefe();
            if (jefe.getId() == 0) {
                ps = da.conectar().prepareStatement("update empleados set primer_nombre=?,segundo_nombre=?,"
                        + " primer_apellido=?,segundo_apellido=?,correo=?,tel_fijo=?,tel_movil=?,jefe=null where id=?");
                ps.setString(1, e.getPrimerNombre());
                ps.setString(2, e.getSegundoNombre());
                ps.setString(3, e.getPrimerApellido());
                ps.setString(4, e.getSegundoApellido());
                ps.setString(5, e.getCorreo());
                ps.setString(6, e.getTelFijo());
                ps.setString(7, e.getTelMovil());
                ps.setInt(8, e.getId());
            } else {

               ps = da.conectar().prepareStatement("update empleados set primer_nombre=?,segundo_nombre=?,"
                        + " primer_apellido=?,segundo_apellido=?,correo=?,tel_fijo=?,tel_movil=?,jefe=? where id=?");
                ps.setString(1, e.getPrimerNombre());
                ps.setString(2, e.getSegundoNombre());
                ps.setString(3, e.getPrimerApellido());
                ps.setString(4, e.getSegundoApellido());
                ps.setString(5, e.getCorreo());
                ps.setString(6, e.getTelFijo());
                ps.setString(7, e.getTelMovil());
                ps.setInt(8, jefe.getId());
                ps.setInt(9, e.getId());
            }
            ps.executeUpdate();
            return true;
        } catch (Exception ex) {
            System.out.println("Error dao " + ex.getMessage());
            return false;
        }
    }

}
