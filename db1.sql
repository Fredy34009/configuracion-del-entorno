create database db1;
use db1;

create table empleados(
id int primary key auto_increment not null,
primer_nombre varchar(50) not null,
segundo_nombre varchar(50) not null,
primer_apellido varchar(50) not null,
segundo_apellido varchar(50) not null,
correo varchar(50) not null,
tel_fijo varchar(12) ,
tel_movil varchar(50),
jefe int,
key(jefe),
foreign key(jefe) references empleados(id)
);